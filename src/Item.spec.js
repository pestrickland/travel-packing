import Item from "./Item.svelte";
import { cleanup, render } from "@testing-library/svelte";

describe("Item", () => {
  const categoryId = 1;
  const dnd = {};
  const item = { id: 2, name: "socks", packed: false };

  afterEach(cleanup);

  test("should render", () => {
    const { getByTestId, getByText } = render(Item, { categoryId, dnd, item });
    const checkbox = document.querySelector('input[type="checkbox"]');
    expect(checkbox).not.toBeNull(); // Found checkbox
    expect(getByText(item.name)); // Found item name
    expect(getByTestId("delete")); // Found delete button
  });

  test("should match snapshot", () => {
    const { container } = render(Item, { categoryId, dnd, item });
    expect(container).toMatchSnapshot();
  });
});
